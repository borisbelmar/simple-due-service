import { NowRequest, NowResponse } from '@vercel/node'
import { tokenVerify } from '../utils/jwt'

export default async (req: NowRequest, res: NowResponse) => {
  const { authorization } = req.headers
  try {
    if (authorization && tokenVerify(authorization)) {
      res.json({ protected: true, message: 'Your token is valid!' })
      return
    } else {
      res.status(403).send('INVALID_TOKEN')
    }
  } catch (err) {
    res.status(403).send('INVALID_TOKEN')
  }
}
import { NowRequest, NowResponse } from '@vercel/node'
import bcrypt from 'bcryptjs'
import IUser from '../../interfaces/IUser'
import User from '../../models/User'
import connectMongoose from '../../services/connectMongoose'
import { tokenSign } from '../../utils/jwt'

export default async (req: NowRequest, res: NowResponse) => {
  if (req.method === 'POST') {
    const user: IUser = req.body
    if (user) {
      await connectMongoose()
      const verifiedUser: IUser = await User.findOne({ email: user.email })
      if (verifiedUser) {
        const passMatch = await bcrypt.compare(user.password, verifiedUser.password)
        if (passMatch) {
          res.json({
            accessToken: tokenSign(verifiedUser)
          })
          return
        }
      }
      res.status(401).send('INVALID_CREDENTIALS')
      return
    }
    res.status(400).send('INVALID_BODY')
  } else {
    res.status(405).send('METHOD_NOT_ALLOWED')
  }
}
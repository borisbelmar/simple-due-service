import { NowRequest, NowResponse } from '@vercel/node'
import bcrypt from 'bcryptjs'
import IUser from '../../interfaces/IUser'
import User from '../../models/User'
import connectMongoose from '../../services/connectMongoose'

export default async (req: NowRequest, res: NowResponse) => {
  if (req.method === 'POST') {
    const user: IUser = req.body
    if (user) {
      try {
        await connectMongoose()
        const salt = await bcrypt.genSalt(10)
        user.password = await bcrypt.hash(user.password, salt)
        const newUser = new User(user)
        await newUser.save()
        res.status(201).json(newUser)
      } catch (err) {
        if (err.code === 11000) {
          res.status(409).send('CONFLICT')
        } else {
          console.log(err.message)
          res.status(500).send(`INTERNAL SERVER ERROR: ${err.message}`)
        }
      }
    } else {
      res.status(400).send('INVALID_BODY')
    }
  } else {
    res.status(405).send('METHOD_NOT_ALLOWED')
  }
}
import { NowRequest, NowResponse } from '@vercel/node'

export default async (_req: NowRequest, res: NowResponse) => {
  res.json({ message: 'Welcome to Simple Due' })
}
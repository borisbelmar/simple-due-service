import jwt from 'jsonwebtoken'
import IUser from '../interfaces/IUser'

const PRIVATE_KEY = process.env.SECRET_KEY

export const tokenVerify = (bearerToken: String): boolean => {
  if (bearerToken.includes('Bearer')) {
    const token = bearerToken.split(' ')[1]
    return jwt.verify(token, PRIVATE_KEY)
  }
  throw Error('INVALID_TOKEN')
}

export const tokenSign = (user: IUser): String => {
  const tokenPayload = {
    sub: user.id
  }
  return jwt.sign(tokenPayload, PRIVATE_KEY, { expiresIn: '1h' })
}
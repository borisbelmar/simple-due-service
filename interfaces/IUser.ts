interface IUser {
  id?: String,
  email: String,
  password: String,
  firstName?: String,
  lastName?: String
}

export default IUser